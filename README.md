# Hangman Game

![https://docs.python.org/3/](https://img.shields.io/badge/Python-3.8%20%7C%203.9%20%7C%203.10-green?style=plastic&logo=python) ![Terminal](https://img.shields.io/badge/Terminal%20Interface-blue) 

## Descripción

Este juego del ahorcado fue desarollado por @gustavo-arboleda con el propósito de practicar conceptos y significados que recién se están aprendiendo, y de paso practicar conceptos básicos de programación con Python.

Este es un juego orientado para desarrolladores que quieran aprender y practicar elementos propios de la programación funcional y la programación orientada a objetos trabajando en conjunto. y para desarolladores amantes del uso de la terminal ya que la interfaz mediante la cual se interactúa co neste juego es la linea de comandos.


## Descárgalo en:

- Clona el repositorio por HTTPS usando:  
    https://gitlab.com/gustavo-arboleda/hangman-game.git

- Descarga el código fuente desde la página de relases:  
    https://gitlab.com/gustavo-arboleda/hangman-game/-/releases
    
## ¿Cómo funciona?

El juego trae una colección de palabras predeterminadas que tienen que ver con diversos conceptos de la programación, pero se pueden configurar las palabras y significados de preferencia en el archivo json llamado `src/words.json`.

Existen dos modos de juego:  

- El primer modo elige una palabra al azar y mostrar su significado, el jugador deberá adivinar la palabra que se está mostrando, si adivina la palabra se le dará una puntuación y se felicitará al jugador, si no adivina la palabra se le irá dibujando una parte del cuerpo del ahorcado y si se dibuja todo el cuerpo el jugador perderá la partida con la opción de repetir la palabra o regresar al menú principal para seguir jugando.

- El segundo modo elige una palabra al azar y solo muestra un indicio de la cantidad de letras que tiene la palabra, el jugador deberá adivinar la palabra que se está mostrando, si adivina la palabra se le dará una puntuación y se le mostrará  la palabra y su significado, si no adivina la palabra se le irá dibujando una parte del cuerpo del ahorcado y si se dibuja todo el cuerpo el jugador perderá la partida con la opción de repetir la palabra o regresar al menú principal para seguir jugando.

Si el jugador adivina la palabra sin comenter errores se le ofrece una felicitación adicional mostrando emogis que hacen más divertido el juego.

## Guía de instalación

### Requerimientos

- Python 3.8 o superior
- Git
- PIP
- Se recomienda usar para la terminal la fuente MesloLGS NF Bold o alguna fuente que tenga soporte para eimojs.

### Pasos para la instalación

1. Clonar el repositorio en el directorio de preferencia ejecutando el comando:  
    `git clone https://gitlab.com/gustavo-arboleda/hangman-game.git`

2. Posicionarse en el directorio del proyecto: `cd hangman-game`

3. Crear un entorno virtual:  
    Para Ubuntu: `python3 -m venv environment`

4. Activar el entorno virtual:  
    Para Ubuntu: `source environment/bin/activate`

5. Instalar las dependencias del proyecto: `pip install -r requirements.txt`


6. Ejecutar el juego: `python3 src/main.py`

7. Disfrutar del juego!! 🤓🎉


## Crea tu propio glosario de palabras

- En `src/words.json` puedes encontrar un glosario de palabras en formato clave valor y puedes modificarlo para tener allí las palabras de tu preferencia y que tu quieras practicar.

## Imágenes de la interfaz

### Mientras esté jugando
![While the game](/images/1.png)

### Cuando pierde el juego 
![Game Over](/images/2.png)

### Cuando gana el juego 
![Win the Game](/images/3.png)

### Acerca del juego 
![About the Game](/images/4.png)