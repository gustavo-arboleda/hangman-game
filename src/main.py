import json
from random import random
from hangman import Hangman
import os

word_dict: dict
game_type: int
word_position: int


def init_the_game(repeat_word: bool = False):
    """
    Inicializa el juego
    """
    global word_dict

    # Obtiene la palabra aleatoria
    word_dict = get_a_word(repeat_word)
    hangman = Hangman(word_dict["word"])
    read_words(hangman)


def restart_screen(hangman: Hangman):
    """Limpia la pantalla y llama a la palabra oculta cada vez"""
    os.system("clear")
    print(hangman.covered_word)


def get_a_word(repeat_word: bool):
    """
    Busca en el archivo presente como BD y retorna un diccionario con la palabra y su
    significado
    """
    global word_position

    # Establece la ruta del archivo
    dir_name = os.path.dirname(__file__)
    file_name = os.path.join(dir_name, "words.json")

    with open(file_name, encoding="utf-8") as file:
        data = json.loads(file.read())

        # Calcula la posición de la nueva palabra
        if not repeat_word:
            word_position = int(random() * len(data["words"]))
        index = 0
        for row in data["words"]:
            if index == word_position:
                items = list(row.items())
                return {
                    "word": items[0][0],
                    "meaning": items[0][1],
                }
            index += 1


def read_words(hangman: Hangman):
    """
    Lógica de validacíón de las letras ingresadas
    """
    global game_type
    global word_dict
    try:
        restart_screen(hangman)
        # Imprime la definición de la palabra
        if game_type == 1:
            print("\n", word_dict["meaning"], sep="")

        # Imprime la gráfica del hombre colgado
        hangman.fails_handler()
        print(hangman.draw_scene)

        if hangman.faults >= hangman.max_faults:
            input("Game Over...\n")
            game_over(hangman)
        else:
            usr_letter = input("\nType a letter...: ")

        # Valida si la letra ingresada existe en la palabra
        if not hangman.is_leter_exists(usr_letter):
            # Incrementa las fallas
            hangman.faults += 1

            # Reduce el puntaje
            if hangman.faults == 1:
                hangman.reduce_score(300)
            else:
                hangman.reduce_score()

            # Reinicia el proceso
            read_words(hangman)
        else:
            hangman.uncover_a_letter(usr_letter)
            # Si la palabra ya ha sido descubierta por completo
            if hangman.is_the_word_uncovered():
                game_won(hangman)
            else:
                read_words(hangman)

    except KeyboardInterrupt:
        main_menu()


def main_menu():
    """
    Menú principal
    """
    global game_type
    user_input = 0

    menu_msg = """

        **      **  *********  **     **  *********  **      **  *********  **     **
        **      **  **     **  ** *   **  **         ** *  * **  **     **  ** *   **
        **********  **     **  **  *  **  **  *****  **  *   **  **     **  **  *  **
        **      **  *********  **   * **  **     **  **      **  *********  **   * **
        **      **  **     **  **    ***  *********  **      **  **     **  **    ***

                        **      **  *********  **     **  **      **
                        ** *  * **  **         ** *   **  **      **
                        **  *   **  *******    **  *  **  **      **
                        **      **  **         **   * **  **      **
                        **      **  *********  **    ***  *********
    """
    options = """
        1. Play (Description)

        2. Play (Only the word)

        3. About

        0. Exit
    """

    os.system("clear")
    print(menu_msg)
    print(options)

    try:
        user_input = int(input(""))
    except ValueError:
        main_menu()
    except KeyboardInterrupt:
        print("\nCome back soon! 🥳")
        exit()

    if user_input == 1:
        game_type = 1
    elif user_input == 2:
        game_type = 2
    elif user_input == 3:
        about()
        main_menu()
    elif user_input == 0:
        exit()

    # Inicializa el juego
    init_the_game()


def game_won(hangman: Hangman):
    """
    Pantalla de juego ganado
    """
    global word_dict
    game_won_msg = """

        **      **  ********  **     **    **  **
        **      **     **     ** *   **    **  **
        **  *   **     **     **  *  **    **  **
        ** *  * **     **     **   * **
        **      **  ********  **    ***    **  **
    """

    os.system("clear")
    print(game_won_msg)
    # Muestra la palabra y su significado

    try:
        print("\n\nWord:✍️")
    except ValueError:
        print("\n\nWord:")
    print(hangman.word, "\n")

    try:
        print("Meaning:📖")
    except ValueError:
        print("Meaning:")
    print(word_dict["meaning"], "\n")

    # Muestra el puntaje del usuario
    try:
        print("Score: 💯")
    except ValueError:
        print("Score:")
    print(hangman.score, "\n")

    if hangman.score == 1000:
        try:
            print("Congratulations👏👏!!\nYou got the highest score 🥳🤓💻")
        except ValueError:
            print("Congratulations!!\nYou got the highest score ;)")

    input("\n\nPress any key to continue\n")
    main_menu()


def game_over(hangman: Hangman):
    """
    Pantalla de juego perdido
    """
    game_over_msg = """

        *********  *********  **      **  *********
        **         **     **  ** *  * **  **
        **  *****  **     **  **  *   **  *******
        **     **  *********  **      **  **
        *********  **     **  **      **  *********

        *********  *       *  *********  *********
        **     **   *     *   **         **     **
        **     **    *   *    *******    ** ******
        **     **     * *     **         **  **
        *********      *      *********  **     **
    """
    os.system("clear")
    print(game_over_msg)
    # Muestra el puntaje del usuario
    print("\n\nScore:")
    print(hangman.score, "\n")
    try:
        print("Better luck for next 🤞💪...")
    except ValueError:
        print("Better luck for next...")

    print(
        """
        Want to repeat the word?

        1. Yes

        0. Main Menu
        """
    )
    try:
        user_input = int(input(""))
    except ValueError:
        main_menu()
    if user_input == 1:
        init_the_game(True)
    else:
        main_menu()


def about():
    """Muestra mensaje de 'acerca de' del Juego"""
    about_msg = """
             **      **  *********  ********    *********
             ** *  * **  **     **  **      **  **
             **  *   **  **     **  **      **  *******
             **      **  *********  **      **  **
             **      **  **     **  ********    *********

              **      **  ********  ********  **      **
              **      **     **        **     **      **
              **  *   **     **        **     **********
              ** *  * **     **        **     **      **
              **      **  ********     **     **      **
    """
    heart_msg = """
                00000000000           000000000000
              00000000     00000   000000     0000000
            0000000             000              00000
           0000000               0                 0000
          000000                                    0000
          00000                                      0000
         00000                                      00000
         00000                                     000000
          000000                                 0000000
           0000000                              0000000
             000000                            000000
               000000                        000000
                  00000                     0000
                     0000                 0000
                       0000             000
                         000         000
                            000     00
                              00  00
                                00
    """
    os.system("clear")
    print(about_msg)
    print(heart_msg)
    try:
        print("By: Gustavo Arboleda 💙🤓⌨️")
    except ValueError:
        print("By: Gustavo Arboleda...")
    print("Email: gustavoandres.arboleda@gmail.com")
    print("\n")
    print("Press return to continue...")
    try:
        input("")
    except KeyboardInterrupt:
        main_menu()


if __name__ == "__main__":
    """Punto de entrada del programa"""
    main_menu()
