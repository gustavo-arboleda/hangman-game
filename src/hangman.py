class Hangman:
    word: str
    covered_word: str
    draw_scene: str
    faults: int = 0
    max_faults: int = 8
    score: int = 1000

    def __init__(self, word: str) -> None:
        self.word = word
        self._init_covered_word()

    def _init_covered_word(self):
        """
        Inicializa la palabra oculta con '_' en función de la palabra dada
        """
        covered_word = ""
        for _ in self.word:
            covered_word += "_ "
        self.covered_word = covered_word

    def is_leter_exists(self, letter: str):
        """
        Valida si una letra existe dentro de la palabra
        """
        if self.word.upper().__contains__(letter.upper()):
            return True
        else:
            return False

    def is_a_letter_uncovered(self, incoming_letter: str):
        """
        Valida si una letra ya ha sido descubierta
        """
        for letter in self.covered_word:
            if letter.upper() == incoming_letter.upper():
                return True
        return False

    def is_the_word_uncovered(self):
        """Valida si ya ha sido descubierta toda la palabra"""
        return self.covered_word.replace(" ", "") == self.word.replace(" ", "")

    def uncover_a_letter(self, usr_letter: str):
        """
        Destapa una letra de la palabra oculta
        """
        covered_word = ""
        for letter in self.word:
            if letter.upper() == usr_letter.upper():
                covered_word += f"{letter} "
            elif self.is_a_letter_uncovered(letter):
                covered_word += f"{letter} "
            else:
                covered_word += "_ "
        self.covered_word = covered_word

    def reduce_score(self, cant_to_reduce: int = 100):
        """Reduce la cantidad dada al score"""
        self.score -= cant_to_reduce
        if self.score < 0:
            self.score = 0

    def fails_handler(self):
        """
        Dibuja el hombre colgado en función de las faltas
        """
        scaffold = """
             X=============x============X
            ||                          ||
            ||                          ||
            ||                          ||
            ||                          ||
            ||                          ||
            ||                          ||
            ||                          ||
            ||                          ||
            ||                          ||
            ||                          ||
            ||                          ||
            ||                          ||
            ||                          ||
            ||                          ||
            ========@            @========
            ||                          ||
            ||                          ||
          __||__                      __||__
        __________                  __________
        """
        # ──────────                  ──────────

        head = (
            (2, 27, "|"),
            (3, 27, "|"),
            (4, 26, "_"),
            (4, 27, "_"),
            (4, 28, "_"),
            (5, 24, "|"),
            (5, 26, "."),
            (5, 28, "."),
            (5, 30, "|"),
            (6, 25, "\\"),
            (6, 27, "_"),
            (6, 29, "/"),
        )

        torso = (
            (8, 26, "|"),
            (8, 28, "|"),
            (9, 26, "|"),
            (9, 28, "|"),
            (10, 26, "|"),
            (10, 28, "|"),
            (11, 26, "|"),
            (11, 28, "|"),
        )
        left_arm = (
            (9, 23, "/"),
            (9, 24, "/"),
            (8, 24, "/"),
            (8, 25, "/"),
            (7, 25, "/"),
        )
        right_arm = (
            (7, 29, "\\"),
            (8, 29, "\\"),
            (8, 30, "\\"),
            (9, 30, "\\"),
            (9, 31, "\\"),
        )
        left_leg = (
            (12, 25, "/"),
            (12, 26, "/"),
            (13, 24, "/"),
            (13, 25, "/"),
            (14, 23, "/"),
            (14, 24, "/"),
        )
        right_leg = (
            (12, 29, "\\"),
            (12, 28, "\\"),
            (13, 29, "\\"),
            (13, 30, "\\"),
            (14, 30, "\\"),
            (14, 31, "\\"),
        )
        tramp_closed = (
            (16, 21, "="),
            (16, 22, "="),
            (16, 23, "="),
            (16, 24, "="),
            (16, 25, "="),
            (16, 26, "="),
            (16, 27, "="),
            (16, 28, "="),
            (16, 29, "="),
            (16, 30, "="),
            (16, 31, "="),
            (16, 32, "="),
        )

        tramp_opened = (
            # Borra la trampa cerrada
            (16, 21, " "),
            (16, 22, " "),
            (16, 23, " "),
            (16, 24, " "),
            (16, 25, " "),
            (16, 26, " "),
            (16, 27, " "),
            (16, 28, " "),
            (16, 29, " "),
            (16, 30, " "),
            (16, 31, " "),
            (16, 32, " "),
            # Escribe la trampa abierta
            (16, 21, "\\"),
            (17, 21, "\\"),
            (17, 22, "\\"),
            (18, 22, "\\"),
            (18, 23, "\\"),
            (19, 23, "\\"),
            (19, 24, "\\"),
            (20, 24, "\\"),
            (20, 25, "\\"),
        )

        dead_animation = ((5, 26, "x"), (5, 28, "x"))

        # Establece las coordenadas del dibujo
        coordinates = []
        if self.faults >= 1:
            coordinates.extend(head)
        if self.faults >= 2:
            coordinates.extend(torso)
        if self.faults >= 3:
            coordinates.extend(left_arm)
        if self.faults >= 4:
            coordinates.extend(right_arm)
        if self.faults >= 5:
            coordinates.extend(left_leg)
        if self.faults >= 6:
            coordinates.extend(right_leg)
        if self.faults >= 7:
            coordinates.extend(tramp_closed)
        if self.faults >= 8:
            coordinates.extend(tramp_opened)
            coordinates.extend(dead_animation)

        # Separa el dibujo en lineas
        rows = [list(row) for row in scaffold.splitlines()]

        for coord in coordinates:
            # Reordena los caracteres en función de las coordenadas
            rows[coord[0]][coord[1]] = coord[2]

        # Dibuja según las coordenadas establecidas
        self.draw_scene = "\n".join(["".join(line) for line in rows])
